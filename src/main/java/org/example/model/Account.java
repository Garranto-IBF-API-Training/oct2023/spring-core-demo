package org.example.model;

import org.springframework.stereotype.Component;

@Component(value = "acc")
public class Account {

    private int accountID;
    private String accountType;

    private double balance;

    public Account(int accountID, String accountType, double balance) {
        System.out.println("Account created using 3-arg constructor ");
        this.accountID = accountID;
        this.accountType = accountType;
        this.balance = balance;
    }

    public Account(){
        System.out.println("Account created using no-arg constructor ");
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountID=" + accountID +
                ", accountType='" + accountType + '\'' +
                ", balance=" + balance +
                '}';
    }
}


//accountid
//accountType
//balance

//Encapsilation

//pojo class ---

//new Account();
//new Account(100,"Savings",2000.23)
//new Account(200,"Savings")

//autowiring
//constructor autowiring

package org.example.config;

import org.example.model.Account;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = "org.example")
public class BeanConfigurations {


//    @Bean(value = "acc")
////    @Lazy(value = true)
//    public Account accountOne(){
//        return new Account();
//    }

}


//    <bean id="accountOne" class="org.example.model.Account" lazy-init="true">
//        <property name="accountID" value="10011" />
//
//    </bean>
//
//    <bean id="accountTwo" class="org.example.model.Account" lazy-init="true">
//        <constructor-arg name="accountID" value="10012" />
//        <constructor-arg name="accountType" value="Loan" />
//        <constructor-arg name="balance" value="1500.15" />
//    </bean>
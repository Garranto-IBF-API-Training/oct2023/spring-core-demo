package org.example;

import org.example.config.BeanConfigurations;
import org.example.model.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );


        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigurations.class);

        Account accountOne = context.getBean("acc",Account.class);
////        Account accoutOne = (Account) context.getBean("accountOne");
        accountOne.setAccountID(120012);
        accountOne.setAccountType("Credit");
        accountOne.setBalance(1200.90);
        System.out.println(accountOne);

    }
}



//classes of data integrations
//classes provide configurations
//classes core logic
//classes represents the data - model
//classes managing the application flow - controller

//account ---

//eager-loading
//lazy initialization

//        creating a context(container(of beans)) using an xml file that contains the bean definitions
//        by default the container will be created with all the beans(except lazily initialized beans) defined (eager loading)

//    ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
//
//    //        getting the account object(bean) from application context
////        context will return the account object as object type
////        downcast the account object returned as object to account type and store in a account reference for further use
//    Account accoutOne = (Account) context.getBean("accountOne");
//        System.out.println(accoutOne);
//
////        getting accountTwo from the context
////        since this bean in lazy initialized it will created by the bean only after the below line
//                Account accoutTwo = (Account) context.getBean("accountTwo");
//                System.out.println(accoutTwo);